<?php

include('db_connect.php');

if (isset($_POST["json"])) {
    $jsonResult = array();
    $data = json_decode($_POST["json"], true);

    // Create the query.
    $id = $data['id'];
    $firstName = $data['first_name'];
    $lastName = $data['last_name'];
    $email = $data['email'];
    $address = $data['address'];
    $city = $data['city'];
    $postalCode = $data['postal_code'];
    $phone = $data['phone'];

    $sql = "UPDATE users SET first_name=$firstName, last_name=$lastName, email=$email, phone=$phone, address=$address,
            city=$city, postal_code=$postalCode WHERE id=$id";

    if ($mysqli->query($sql) === TRUE) {
        $stmt = $mysqli->prepare("SELECT id, username,email, first_name, last_name, address, city, postal_code, phone, user_role, payment_type FROM users WHERE id=$id");
        // Secure the statement against injection attacks.
        //$stmt->bind_param('i', $id);

        // Get user variables and add security to password.
       // $username = trim($data['username']);
       // $password = trim($data['password']);
        // Execute query.
        $stmt->execute();
        // Used for counting the number of rows.
        $stmt->store_result();
        // Count rows.
        $rowCount = $stmt->num_rows;
        // Secure the result set.
        $username = "";
        $firstName = "";
        $lastName = "";
        $address = "";
        $phone = "";
        $paymentType = "";
        $city = "";
        $postalCode = "";
        $id = "";
        $role = "";
        $email = "";
        $balance = "";
        $stmt->bind_result($id, $username, $email, $firstName, $lastName, $address, $city, $postalCode, $phone, $role, $paymentType);
        // Get the result array.
        $stmt->fetch();


        // If a user with the correct credentials is found, set their id for use in the system, else return them to the login page.
        $updateResponse = array();
        if ($rowCount == 1) {
            $updateResponse['id'] = $id;
          //  $updateResponse['username'] = $data['username'];
            $updateResponse['email'] = $email;
           // $updateResponse['password'] = $data['password'];
            $updateResponse['first_name'] = $firstName;
            $updateResponse['last_name'] = $lastName;
            $updateResponse['address'] = $address;
            $updateResponse['city'] = $city;
            $updateResponse['postal_code'] = $postalCode;
            $updateResponse['phone'] = $phone;
            $updateResponse['card_type'] = $paymentType;
            $updateResponse['user_role'] = $role;
            $updateResponse['balance'] = '$' . $balance;
        }
        // Close statement.
        $stmt->close();
        // Close connection.
        $mysqli->close();
        $jsonResult['update'] = $updateResponse;
        echo (json_encode($jsonResult));
    } else {
        $msyqli->close();
        $jsonResult['update'] = array();
        echo (json_encode($jsonResult));
    }
}
?>