-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for sleepeasydesign
CREATE DATABASE IF NOT EXISTS `sleepeasydesign` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sleepeasydesign`;


-- Dumping structure for table sleepeasydesign.accounts
CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `balance` float NOT NULL DEFAULT '0',
  `updated` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_accounts_users` (`user_id`),
  CONSTRAINT `FK_accounts_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table sleepeasydesign.accounts: ~3 rows (approximately)
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
INSERT INTO `accounts` (`id`, `user_id`, `balance`, `updated`) VALUES
	(11, 13, 952.1, '2015-02-16 18:21:13'),
	(12, 14, 0, '2015-02-16 16:55:40'),
	(13, 15, 0, '2015-02-16 18:26:59'),
	(14, 16, 0, NULL),
	(15, 17, 0, NULL),
	(16, 18, 0, NULL);
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.charges
CREATE TABLE IF NOT EXISTS `charges` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `account_id` int(10) NOT NULL,
  `charge_type` varchar(50) NOT NULL,
  `charge_amount` float NOT NULL DEFAULT '0',
  `charge_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `charge_comment` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK__users` (`user_id`),
  KEY `FK_charges_accounts` (`account_id`),
  KEY `FK_charges_charge_types` (`charge_type`),
  CONSTRAINT `FK__users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_charges_accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_charges_charge_types` FOREIGN KEY (`charge_type`) REFERENCES `charge_types` (`title`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table sleepeasydesign.charges: ~12 rows (approximately)
/*!40000 ALTER TABLE `charges` DISABLE KEYS */;
INSERT INTO `charges` (`id`, `user_id`, `account_id`, `charge_type`, `charge_amount`, `charge_datetime`, `charge_comment`) VALUES
	(10, 14, 12, 'Room Booking', 179.99, '2015-02-16 13:19:44', NULL),
	(11, 13, 11, 'Room Booking', 300, '2015-02-16 13:49:58', NULL),
	(12, 13, 11, 'Room Service', 22.22, '2015-02-16 13:50:24', NULL),
	(13, 13, 11, 'Room Service', 200.12, '2015-02-16 14:34:12', NULL),
	(14, 14, 12, 'Room Booking', 179.99, '2015-02-16 14:57:02', NULL),
	(15, 14, 12, 'Room Booking', 719.96, '2015-02-16 15:01:57', NULL),
	(16, 13, 11, 'Room Service', 300, '2015-02-16 15:02:38', NULL),
	(17, 14, 12, 'Room Booking', 200, '2015-02-16 16:42:16', NULL),
	(18, 13, 11, 'Room Booking', 200, '2015-02-16 16:42:52', NULL),
	(19, 14, 12, 'Room Booking', 149.99, '2015-02-16 16:55:34', NULL),
	(20, 13, 11, 'Room Booking', 251.98, '2015-02-16 18:21:13', NULL),
	(21, 15, 13, 'Room Booking', 179.99, '2015-02-16 18:22:03', NULL);
/*!40000 ALTER TABLE `charges` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.charge_types
CREATE TABLE IF NOT EXISTS `charge_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table sleepeasydesign.charge_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `charge_types` DISABLE KEYS */;
INSERT INTO `charge_types` (`id`, `title`, `description`) VALUES
	(1, 'Room Service', 'Room Service Charge'),
	(2, 'Room Booking', 'Room Charges');
/*!40000 ALTER TABLE `charge_types` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.payments
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL DEFAULT '0',
  `account_id` int(10) NOT NULL DEFAULT '0',
  `payment_amount` float NOT NULL DEFAULT '0',
  `payment_comment` varchar(100) DEFAULT NULL,
  `payment_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_payments_users` (`user_id`),
  KEY `FK_payments_accounts` (`account_id`),
  CONSTRAINT `FK_payments_accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_payments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COMMENT='This will hold all payments';

-- Dumping data for table sleepeasydesign.payments: ~10 rows (approximately)
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` (`id`, `user_id`, `account_id`, `payment_amount`, `payment_comment`, `payment_datetime`) VALUES
	(1, 14, 12, 179.99, NULL, '2015-02-16 14:15:30'),
	(2, 14, 12, 0, NULL, '2015-02-16 14:19:14'),
	(3, 14, 12, 0, NULL, '2015-02-16 14:20:47'),
	(4, 14, 12, 0, NULL, '2015-02-16 14:21:13'),
	(5, 14, 12, 0, NULL, '2015-02-16 14:23:58'),
	(6, 14, 12, 0, NULL, '2015-02-16 14:25:08'),
	(7, 14, 12, 179.99, NULL, '2015-02-16 14:57:25'),
	(8, 14, 12, 719.96, NULL, '2015-02-16 15:02:15'),
	(9, 14, 12, 349.99, NULL, '2015-02-16 16:55:40'),
	(10, 15, 13, 179.99, NULL, '2015-02-16 18:26:59');
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.payment_types
CREATE TABLE IF NOT EXISTS `payment_types` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `card_type` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`card_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table sleepeasydesign.payment_types: ~3 rows (approximately)
/*!40000 ALTER TABLE `payment_types` DISABLE KEYS */;
INSERT INTO `payment_types` (`id`, `card_type`) VALUES
	(3, 'AMERICAN EXPRESS'),
	(2, 'MASTERCARD'),
	(1, 'VISA');
/*!40000 ALTER TABLE `payment_types` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.reservations
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `room_type` int(10) NOT NULL,
  `checkin` date NOT NULL,
  `checkout` date NOT NULL,
  `number_of_guests` int(10) NOT NULL DEFAULT '1',
  `discount` float NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `room_number` int(10) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_reservations_users` (`user_id`),
  KEY `FK_reservations_room_types` (`room_type`),
  KEY `FK_reservations_rooms` (`room_number`),
  KEY `FK_reservations_accounts` (`account_id`),
  CONSTRAINT `FK_reservations_accounts` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reservations_room_types` FOREIGN KEY (`room_type`) REFERENCES `room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reservations_rooms` FOREIGN KEY (`room_number`) REFERENCES `rooms` (`room_number`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_reservations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1 COMMENT='this table will be used to hold the hotel reservations';

-- Dumping data for table sleepeasydesign.reservations: ~3 rows (approximately)
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` (`id`, `user_id`, `room_type`, `checkin`, `checkout`, `number_of_guests`, `discount`, `status`, `room_number`, `account_id`) VALUES
	(27, 13, 3, '2015-02-16', '2015-02-17', 1, 0, 0, 200, 11),
	(29, 13, 2, '2015-02-18', '2015-02-20', 1, 0, 1, 101, 11),
	(30, 15, 3, '2015-02-16', '2015-02-17', 1, 0, 0, 201, 13);
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `room_number` int(10) NOT NULL DEFAULT '100',
  `room_type` int(10) NOT NULL,
  PRIMARY KEY (`room_number`),
  KEY `FK_rooms_room_types` (`room_type`),
  CONSTRAINT `FK_rooms_room_types` FOREIGN KEY (`room_type`) REFERENCES `room_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table sleepeasydesign.rooms: ~6 rows (approximately)
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`room_number`, `room_type`) VALUES
	(1000, 1),
	(100, 2),
	(101, 2),
	(200, 3),
	(201, 3),
	(3003, 3);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.room_types
CREATE TABLE IF NOT EXISTS `room_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `description` varchar(100) NOT NULL DEFAULT '0',
  `cost` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table sleepeasydesign.room_types: ~3 rows (approximately)
/*!40000 ALTER TABLE `room_types` DISABLE KEYS */;
INSERT INTO `room_types` (`id`, `title`, `description`, `cost`) VALUES
	(1, 'Executive Room', '0', 149.99),
	(2, 'Standard Room', '0', 125.99),
	(3, 'Exclusive Room', '0', 179.99);
/*!40000 ALTER TABLE `room_types` ENABLE KEYS */;


-- Dumping structure for table sleepeasydesign.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `user_role` enum('Guest','Employee','Manager') NOT NULL DEFAULT 'Guest',
  `payment_type` varchar(50) DEFAULT NULL,
  `cardholder_name` varchar(50) NOT NULL,
  `card_number` varchar(50) NOT NULL,
  `card_expiration` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `FK_users_payment_types` (`payment_type`),
  CONSTRAINT `FK_users_payment_types` FOREIGN KEY (`payment_type`) REFERENCES `payment_types` (`card_type`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1 COMMENT='This table will hold the information for the user, including all of the user roles for adminstraion';

-- Dumping data for table sleepeasydesign.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `address`, `postal_code`, `city`, `phone`, `user_role`, `payment_type`, `cardholder_name`, `card_number`, `card_expiration`) VALUES
	(13, 'jstillman', '$2a$10$XbTIuM8w3htcxMKDPharsuo/7//12QPt3fX7KyBZftJypkKVWldgm', 'jstillman05@gmail.com', 'Jordan', 'Stillmans', '426 Bunbury Rd - Route 21', 'C1B3M5', 'Bunbury', '902-569-1413', 'Manager', 'VISA', 'Jordan Stillman', '5181271079384019', '2015-12-01'),
	(14, 'jstillman2', '$2a$10$rhNFHG.BHIQr7G32Cbnsfu9a9hek/pb0GDDcsBdztCGd81NvmyED2', 'jstillman05@gmail.com', 'Jordan', 'Stillman', '426 Bunbury rd', 'c1b3m5', 'bunbury', '9025691413', 'Employee', 'VISA', 'Jordan Stillman', '4519123412341234', '2015-02-01'),
	(15, 'jstillman3', '$2a$10$IrDh6jwTvciNEJ8g.f71FevkhX4eAizpgmo7ARCWQ8CiCuTXgqJ5a', 'jctristar@hotmail.com', 'Charity', 'Stillman', '426 Bunbury Rd. Route #21', 'C1B3M5', 'Bunbury', '9025691413', 'Guest', 'VISA', 'Charity Stillman', '4519123412341234', '2015-02-01'),
	(16, 'manager', '$2a$10$bvefcp1KEI.wErh6yPpwRus7N72l443rWebEmO9L6QdpCosbg8uQe', 'manager@gmail.com', 'Manager', 'Manager', 'manager st', 'C1B3M5', 'Bunbury', '0000000000', 'Manager', 'VISA', 'Charity Stillman', '4519123412341234', '2015-02-01'),
	(17, 'employee', '$2a$10$sy6ZPwRSCZP5hqElQskdm.SeJcjpoLynmoTBMfhG0iYccsIQ/PAWK', 'employee@gmail.com', 'employee', 'employee', 'employee', 'employee', 'employee', 'employee', 'Employee', 'VISA', 'employee employee', '4519123412341234', '2015-02-01'),
	(18, 'guests', '$2a$10$Xsz2TsVcW.MgAHqLRMbehupSvynqowaLdi3eUfSVSsCeIusxUqy6y', 'Guests@gmail.com', 'Guests', 'Guests', 'Guests', 'Guests', 'Guests', 'Guests', 'Guest', 'VISA', 'Guests Guests', '4519123412341234', '2015-02-01');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Dumping structure for trigger sleepeasydesign.charges_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `charges_after_insert` AFTER INSERT ON `charges` FOR EACH ROW BEGIN
UPDATE accounts
SET  balance = balance + NEW.charge_amount
WHERE id = NEW.account_id;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sleepeasydesign.payments_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `payments_after_insert` AFTER INSERT ON `payments` FOR EACH ROW BEGIN
UPDATE accounts
SET balance  = balance - NEW.payment_amount
WHERE id= NEW.account_id;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Dumping structure for trigger sleepeasydesign.users_after_insert
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `users_after_insert` AFTER INSERT ON `users` FOR EACH ROW BEGIN
INSERT INTO accounts(user_id) values(NEW.id);
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
