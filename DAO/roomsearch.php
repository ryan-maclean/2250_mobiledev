<?php

include('db_connect.php');

if (isset($_POST["json"])) {
    $jsonResult = array();
    $data = json_decode($_POST["json"], true);

    // Create the query.
    $stmt = $mysqli->prepare("SELECT r.room_number FROM rooms r WHERE r.room_type =? AND r.room_number NOT IN ( SELECT res.room_number FROM reservations res WHERE NOT (res.checkout > ? OR res.checkin < ? ) ) ORDER BY r.room_number");
    // Secure the statement against injection attacks.
    $stmt->bind_param('sss', $roomType, $checkin,$checkout);

    // Get user variables and add security to password.
    $roomType = trim($data['roomType']);
    $checkin = trim($data['checkin']);
    $checkout = trim($data['checkout']);

    // Execute query.
    $stmt->execute();
    // Used for counting the number of rows.
    //$stmt->store_result();
    // Count rows.
   // $rowCount = $stmt->num_rows;
    // Secure the result set.
    //$room_number = "";
    $stmt->bind_result($room_number);
    // Get the result array.
   // $stmt->fetch();
 	$numResults = $stmt->num_rows;
    $rooms = array();
    // If a user with the correct credentials is found, set their id for use in the system, else return them to the login page.
    //if($rowCount > 0){
        while($stmt->fetch()){
            $room = array($room_number);
            array_push($rooms, $room);
        }
   // }
    // Close statement.
    $stmt->close();
    // Close connection.
    $mysqli->close();
    $jsonResult ['rooms'] = $rooms;
    $jsonResult['roomType']=$data['roomType'];

    $jsonResult['checkin'] = $data['checkin'];
    $jsonResult['checkout'] = $data['checkout'];
    $jsonResult['numberOfGuests']=$data['num_of_guests'];

    echo(json_encode($jsonResult));
}
?> 