<?php

include('db_connect.php');

if (isset($_POST["json"])) {
    $jsonResult = array();
    $data = json_decode($_POST["json"], true);

    // Create the query.
    $stmt = $mysqli->prepare("SELECT id, username,email, first_name, last_name, address, city, postal_code, phone, user_role, payment_type FROM users WHERE username = ? AND password=? ");
    // Secure the statement against injection attacks.
    $stmt->bind_param('ss', $username, $password);

    // Get user variables and add security to password.
    $username = trim($data['username']);
    $password = trim($data['password']);
    // Execute query.
    $stmt->execute();
    // Used for counting the number of rows.
    $stmt->store_result();
    // Count rows.
    $rowCount = $stmt->num_rows;
    // Secure the result set.
	$username = "";
    $firstName = "";
    $lastName = "";
	$address = "";
	$phone = "";
	$paymentType = "";
	$city = "";
	$postalCode = "";
	$id = "";
    $role = "";
	$email = "";
	$balance = "";

    $stmt->bind_result($id, $username, $email, $firstName, $lastName, $address, $city, $postalCode, $phone, $role, $paymentType);

    $accountId = "";

    // Get the result array.
    $stmt->fetch();


    // If a user with the correct credentials is found, set their id for use in the system, else return them to the login page.
    $loginResponse = array();
    if ($rowCount == 1) {
		$loginResponse['id'] = $id;
        $loginResponse['username'] = $data['username'];
		$loginResponse['email'] = $email;
        $loginResponse['password'] = $data['password'];
		$loginResponse['first_name'] = $firstName;
        $loginResponse['last_name'] = $lastName;       
		$loginResponse['address'] = $address;
		$loginResponse['city'] = $city;
		$loginResponse['postal_code'] = $postalCode;
		$loginResponse['phone'] = $phone;		
		$loginResponse['card_type'] = $paymentType;	
		$loginResponse['user_role'] = $role;		
		//$loginResponse['balance'] = '$' . $balance;

        $query = $mysqli->query("SELECT id , balance from accounts WHERE user_id='".$id."'");
        $result = $query->fetch_assoc();


        //$stmt2->bind_result($accountId);
        $loginResponse['account_id'] = $result['id'];
		$loginResponse['balance'] = $result['balance'];
    }
    // Close statement.
    $stmt->close();
    // Close connection.
    $mysqli->close();
    $jsonResult ['login'] = $loginResponse;
    echo(json_encode($jsonResult));
}
?> 