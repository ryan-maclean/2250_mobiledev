<?php

include('db_connect.php');
if (isset($_POST["json"])) {
    $data = json_decode($_POST["json"], true);
	
    // Create the query.
    $stmt = $mysqli->prepare("SELECT username FROM users WHERE username=?");
	
    $stmt->bind_param('s', $username);

    $username = trim($data['username']);

    // Execute query.
    $stmt->execute();
    // Used for counting the number of rows.
    $stmt->store_result();
    // Count rows.
    $rowCount = $stmt->num_rows;
    // Close statement.
    $stmt->close();

    if ($rowCount == 0) {
        // Create the query.
        $stmt = $mysqli->prepare("INSERT INTO users(username, password, email, first_name, last_name, address, postal_code, city, phone, payment_type, cardholder_name, card_number, card_expiration) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)");
        // Secure the statement against injection attacks.
        $stmt->bind_param('sssssssssssss', 
		$username, $password, $email, $firstName, $lastName, $address, $postalCode, $city, $phone,  $paymentType, $cardName, $cardNo, $cardExp);

        // Get user variables
		$username = trim($data['username']);
        $email = trim($data['email']);
        $password = trim($data['password']);
        $lastName = trim($data['last_name']);
        $firstName = trim($data['first_name']);
        $address = trim($data['address']);
        $postalCode = trim($data['postal_code']);
		$city = trim($data['city']);
		$phone = trim($data['phone']);
		$paymentType = trim($data['payment_type']);
		$cardName = trim($data['cardholder_name']);
		$cardNo = trim($data['card_number']);
		$cardExp = trim($data['card_expiration']);



        // Execute query.
        $stmt->execute();
        // Close statement.
        $stmt->close();

         $registerResponse['registerStatus'] = 1;
    } else {
        // Used for the login page and function.  This assumes the user was not created, specifically a username error.
        $registerResponse['registerStatus'] = 0;
    }
    $jsonResult = array();
    $jsonResult ['response'] = $registerResponse;
    echo(json_encode($jsonResult));
    // Close connection.
    $mysqli->close();
}
