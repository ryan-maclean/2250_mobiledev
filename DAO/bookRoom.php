<?php

include('db_connect.php');
if (isset($_POST["json"])) {
    $data = json_decode($_POST["json"], true);
       // $registerResponse;
        // Create the query.
        $stmt = $mysqli->prepare("INSERT INTO reservations(user_id, room_type, checkin, checkout, number_of_guests, room_number, account_id) VALUES (?,?,?,?,?,?,?)");
        // Secure the statement against injection attacks.
        $stmt->bind_param('sssssss',
            $userId ,
            $roomType ,
            $checkin ,
            $checkout ,
            $numGuest ,

            $roomNum ,
            $accountId );

        // Get user variables
        $userId = trim($data['user_id']);
        $roomType = trim($data['room_type']);
        $checkin = trim($data['checkin']);
        $checkout = trim($data['checkout']);
        $numGuest= trim($data['num_of_guests']);
        $roomNum = trim($data['room_number']);
        $accountId = trim($data['account_id']);


    $registerResponse = array();

        // Execute query.
       if( $stmt->execute()){

        // Close statement.
        $stmt->close();

        $registerResponse['status'] = "1";
    } else {
        // Used for the login page and function.  This assumes the user was not created, specifically a username error.
        $registerResponse['status'] = "0";
    }
    $jsonResult = array();
    $jsonResult ['response'] = $registerResponse;
    echo(json_encode($jsonResult));
    // Close connection.
    $mysqli->close();
}
