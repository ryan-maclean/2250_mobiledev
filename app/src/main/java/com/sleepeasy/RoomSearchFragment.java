package com.sleepeasy;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.sleepeasy.activities.AsyncResponse;
import com.sleepeasy.activities.RoomList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RoomSearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RoomSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RoomSearchFragment extends Fragment implements AsyncResponse{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private DatePicker date;
    private DatePicker date2;
    private Spinner spinner;
    private Bundle mBundle;

    private static String userInfo;


    private HashMap<String,String> roomTypes = new HashMap<String,String>();





    private EditText output;
    private EditText output2;



    private int mYear;
    private int mMonth;
    private int mDay;
    static final int DATE_DIALOG_ID = 100;


    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RoomSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RoomSearchFragment newInstance(String param1, String param2) {
        RoomSearchFragment fragment = new RoomSearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public RoomSearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


        }


        roomTypes.put("1","Executive Room");
        roomTypes.put("2","Standard Room");
        roomTypes.put("3","Exclusive Room");





    }




    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
                // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {
            mYear = selectedYear;
            mMonth = selectedMonth;
            mDay = selectedDay;
            // set selected date into Text View
            output.setText(new StringBuilder().append(mMonth + 1)
                            .append("-").append(mDay).append("-").append(mYear).append(" "));
            // set selected date into Date Picker
            date.init(mYear,mMonth, mDay, null);
        }
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room_search, container, false);
        setSpinnerContent( view );
        return view;

    }

    private void setSpinnerContent(View view) {
        spinner = (Spinner) view.findViewById(R.id.noGuests);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity().getApplicationContext(), R.array.guests, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void processFinish(JSONObject jsonResponse) {
        Intent intent = new Intent(getActivity(),RoomList.class);
        //intent.putExtra(getIntent().getStringExtra("info"));
        intent.putExtra("results",jsonResponse.toString());
        intent.putExtra("info",userInfo);

        startActivity(intent);

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Spinner roomTypeSelect = (Spinner)getActivity().findViewById(R.id.typeSelect);
        ArrayAdapter roomTypeList = ArrayAdapter.createFromResource(getActivity().getApplicationContext(),R.array.room_types,android.R.layout.simple_spinner_item);
        roomTypeList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        roomTypeSelect.setAdapter(roomTypeList);


        Button buttonSearch  =(Button)getActivity().findViewById(R.id.buttonSearch);

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                MediaPlayer mp = MediaPlayer.create(getActivity(),R.raw.button2);
                mp.start();
               // Log.d("JSTILLMAN", checkin);

                // Use AsyncTask execute Method To Prevent ANR Problem
                try {
                    Log.d("RYANTEST", "Create JSONObject");
                    JSONObject roomSearch = new JSONObject();

                    int checkinYearInt = ((DatePicker)getActivity(). findViewById(R.id.checkIn)).getYear();
                    String checkinYearString = String.valueOf(checkinYearInt);
                    int checkinMonthInt = ((DatePicker)getActivity(). findViewById(R.id.checkIn)).getMonth()+1;
                    String checkinMonthString = String.valueOf(checkinMonthInt);
                    int checkinDayInt = ((DatePicker)getActivity(). findViewById(R.id.checkIn)).getDayOfMonth();
                    String checkinDayString = String.valueOf(checkinDayInt);


                    if(checkinMonthString.length()==1){
                        checkinMonthString = "0"+checkinMonthString;

                    }
                    if(checkinDayString.length()==1){
                        checkinDayString = "0"+checkinDayString;

                    }

                    String checkin = checkinYearString + "-"+checkinMonthString+"-"+checkinDayString;



                    int checkoutYearInt = ((DatePicker)getActivity(). findViewById(R.id.checkOut)).getYear();
                    String checkoutYearString = String.valueOf(checkoutYearInt);
                    int checkoutMonthInt = ((DatePicker)getActivity(). findViewById(R.id.checkOut)).getMonth()+1;
                    String checkoutMonthString = String.valueOf(checkoutMonthInt);
                    int checkoutDayInt = ((DatePicker)getActivity(). findViewById(R.id.checkOut)).getDayOfMonth();
                    String checkoutDayString = String.valueOf(checkoutDayInt);


                    if(checkoutMonthString.length()==1){
                        checkoutMonthString = "0"+checkoutMonthString;

                    }
                    if(checkoutDayString.length()==1){
                        checkoutDayString = "0"+checkoutDayString;

                    }

                    String checkout = checkoutYearString + "-"+checkoutMonthString+"-"+checkoutDayString;

                   // String checkout = ((DatePicker)getActivity(). findViewById(R.id.checkOut)).getYear() + "-"+((DatePicker)getActivity(). findViewById(R.id.checkOut)).getMonth()+"-"+((DatePicker)getActivity(). findViewById(R.id.checkOut)).getDayOfMonth();
                    String numberOfGuest = ((Spinner)getActivity().findViewById(R.id.noGuests)).getSelectedItem().toString();
                    int roomType = ((Spinner)getActivity().findViewById(R.id.typeSelect)).getSelectedItemPosition()+1;
                    Log.d("JSTILLMAN","ROOM TYPE: "+ String.valueOf(roomType));
                    Log.d("JSTILLMAN","NUMBER OF GUESTS: "+ numberOfGuest);


                    roomSearch.put("checkin", checkin);
                    roomSearch.put("checkout", checkout);
                    roomSearch.put("num_of_guests",numberOfGuest);
                    roomSearch.put("roomType",roomType);

                    JSONPost connect = new JSONPost(getResources().getString(R.string.url) + "roomsearch.php");

                    // From HelmiB, toobsco42 http://stackoverflow.com
                    connect.delegate = RoomSearchFragment.this;

                    connect.execute(new JSONObject[]{roomSearch});

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public static String userInfo(String info){
            userInfo = info;

        return userInfo;
    }


    class JSONPost extends AsyncTask<JSONObject, JSONObject, JSONObject>

    {

        private String url = "";
        public AsyncResponse delegate = null;
        private JSONObject jsonResponse = null;

        public JSONPost(String url) {
        Log.d("RYANTEST", url);
        this.url = url;
    }

        @Override
        protected JSONObject doInBackground(JSONObject... data) {
        try {
            JSONObject json = data[0];
            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);
            HttpPost post = new HttpPost(url);

            StringEntity se = new StringEntity("json=" + json.toString());
            post.addHeader("content-type", "application/x-www-form-urlencoded");
            post.setEntity(se);

            HttpResponse response = client.execute(post);
            String resultFromServer = EntityUtils.toString(response.getEntity());
            Log.d("JSTILLMAN",resultFromServer);
            jsonResponse = new JSONObject(resultFromServer);
            jsonResponse = new JSONObject(resultFromServer);
            jsonResponse.put("databaseError", false);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                jsonResponse = new JSONObject();
                jsonResponse.put("databaseError", true);
                jsonResponse.put("databaseErrorText", "There is no connection to the database, contact the administrator.");
            } catch (Exception db) {
                db.printStackTrace();
            }
        } finally {
            return jsonResponse;
        }
    }

        @Override
        protected void onPostExecute(JSONObject jsonResponse) {
        try {
            Log.d("JSTILLMAN","IN PRoccessFinish");
            // From From HelmiB, toobsco42 http://stackoverflow.com
            JSONObject searchResults;
            searchResults = jsonResponse;
            Log.d("JSTILLMAN",searchResults.getJSONArray("rooms").toString()) ;

        processFinish(searchResults);
         //

//            Bundle bundle = new Bundle();
//            bundle.putString("results",searchResults.toString());
//            frag.setArguments(bundle);
//            FragmentManager fragmentManager = getFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id., frag).commit();


//            FragmentTransaction trans = getFragmentManager().beginTransaction();
//            trans.replace(R.id., frag);
//            trans.addToBackStack(null);
//            trans.commit();













        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    }
}
