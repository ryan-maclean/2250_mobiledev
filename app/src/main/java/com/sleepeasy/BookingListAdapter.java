package com.sleepeasy;

/**
 * Created by jstillman on 2/20/2015.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;


import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;



public class BookingListAdapter extends ArrayAdapter<Booking> {
    Context mContext;
    ArrayList<Booking> mBookings;

    public BookingListAdapter(Context context,ArrayList<Booking> bookings) {
        super(context,R.layout.booking_row,bookings);
        mContext = context;
        mBookings = bookings;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if(convertView ==null){
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.booking_row,null);
            holder = new ViewHolder();
            holder.reservationId = (TextView)convertView.findViewById(R.id.reservationId);
            holder.checkinDate = (TextView)convertView.findViewById(R.id.checkinDate);
            holder.checkoutDate = (TextView)convertView.findViewById(R.id.checkoutDate);
            holder.numGuests = (TextView)convertView.findViewById(R.id.numGuests);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }
        Booking booking = mBookings.get(position);


        holder.reservationId.setText(booking.getId());
        holder.reservationId.setText(booking.getCheckin());
        holder.reservationId.setText(booking.getCheckout());
        holder.reservationId.setText(booking.getNumGuest());


        return convertView;

    }

    private static class ViewHolder {
        TextView reservationId;
        TextView checkinDate;
        TextView checkoutDate;
        TextView numGuests;
    }
}
