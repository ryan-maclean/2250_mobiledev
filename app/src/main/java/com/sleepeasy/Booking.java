package com.sleepeasy;

/**
 * Created by jstillman on 2/20/2015.
 */
public class Booking {

    private String id;
    private String checkin;
    private String checkout;
    private String numGuest;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getNumGuest() {
        return numGuest;
    }

    public void setNumGuest(String numGuest) {
        this.numGuest = numGuest;
    }
}
