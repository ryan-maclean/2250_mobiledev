package com.sleepeasy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * This class provides an interface to the local SQLite database
 * User: Tim FP
 * Date: Feb 19, 2015
 */
public class SQLiteHandler extends SQLiteOpenHelper {
    //Constants for table/column names, etc.
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "sleepEasySession";
    private static final String TABLE_SESSION = "session";
    private static final String KEY_SESSIONID = "session_id";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_SESSIONON = "session_on";

    public SQLiteHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /* Create the database tables */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createSessionTable = "create table " + TABLE_SESSION + " (" + KEY_SESSIONID +
                " integer primary key," + KEY_USERNAME + " text unique," + KEY_PASSWORD +
                " text," + KEY_SESSIONON + " integer);";

        db.execSQL(createSessionTable);
    }

    /* Upgrade the database - drop the old table and re-create */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_SESSION);
        onCreate(db);
    }

    /* Checks if a user already exists */
    public boolean userExists(String username) {
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "select * from " + TABLE_SESSION + " where " + KEY_USERNAME + " = ?";
        Cursor cursor = db.rawQuery(query, new String[] {username});

        if(cursor.getCount() > 0) { return true; }

        return false;
    }

    /* Adds a new user */
    public void addUser(String username, String password){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USERNAME, username);
        values.put(KEY_PASSWORD, password);
        values.put(KEY_SESSIONON, 0);

        db.insert(TABLE_SESSION, null, values);
        db.close();
    }

    /* Turns off all sessions */
    public void turnOffSessions(){
        SQLiteDatabase db = this.getWritableDatabase();

        String query = "update " + TABLE_SESSION + " set " + KEY_SESSIONON + " = 0 where " + KEY_SESSIONON + "=  1";

        db.rawQuery(query, null);
    }

    /* Turn a user's session on */
    public void turnOnSession(String username){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SESSIONON, 1);

        db.update(TABLE_SESSION, values, KEY_USERNAME + " =?", new String[] {username});
    }

    /* Check for an active session */
    public boolean isActive(){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "select * from " + TABLE_SESSION;
        Cursor cursor = db.rawQuery(query, null);

        if(cursor.getCount() > 0) { return true; }

        return false;
    }

    /* Find the username/password of the active session's user */
    public String[] getActiveUser(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SESSION, new String[] {KEY_USERNAME, KEY_PASSWORD}, KEY_SESSIONON + " =?",
                                 new String[] {String.valueOf(1)}, null, null, null, null);

        if (cursor != null) { cursor.moveToFirst(); }

        String[] userInfo = {cursor.getString(0), cursor.getString(1)};
        return userInfo;
    }
}
