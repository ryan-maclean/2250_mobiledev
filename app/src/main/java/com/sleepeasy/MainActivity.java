package com.sleepeasy;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.sleepeasy.activities.Welcome;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The main activity holds a navigation drawer and an empty frame layout to hold the content as fragments.
 * Navigation drawer based on code from developer.android.com & slidenerd on youtube
 * Date: Feb 15, 2015
 * User: Tim FP
 */
public class MainActivity extends ActionBarActivity implements AdapterView.OnItemClickListener,
                                                    RoomSearchFragment.OnFragmentInteractionListener,
                                                    ReservationListFragment.OnFragmentInteractionListener,
                                                    ViewProfileFragment.OnFragmentInteractionListener,
                                                    GuestServicesFragment.OnFragmentInteractionListener
                                                    {
    private String[] drawerListTitles;
    private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawerToggle drawerToggle;
    private JSONObject userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //populate drawer resources
        drawerListTitles = getResources().getStringArray(R.array.drawer_list_titles);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.nav_drawer);

        //set adapter & click listener for drawer list
        drawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, drawerListTitles));
        drawerList.setOnItemClickListener(this);

        //define drawer listener & set it
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view){
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View view){
                super.onDrawerOpened(view);
            }
        };

        drawerLayout.setDrawerListener(drawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }


    /* Item click handler for each menu item */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //choose the proper fragment...
        Fragment fragment = new Fragment();
        switch(position){
            case 0:
                fragment = new RoomSearchFragment();
                    RoomSearchFragment.userInfo(userInfo.toString());


                break;
            case 1:
                fragment = new ReservationListFragment();
                ReservationListFragment.userInfo(userInfo.toString());
                break;
            case 2:
                fragment = new ViewProfileFragment();

                ViewProfileFragment.userInfo(userInfo.toString());


                // Bundle args = new Bundle();
                //args.putString("info", userInfo.toString());
                //fragment.setArguments(args);
                break;
            case 3:
                fragment = new GuestServicesFragment();
                break;
            case 4:
                logout();
                break;
            default:
                fragment = new Fragment();
                break;
        }
        //...and put it into the view
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        //set menu bar title to the fragment's name, set the checked list item, close the nav drawer
        getSupportActionBar().setTitle(drawerListTitles[position]);
        drawerList.setItemChecked(position, true);
        drawerLayout.closeDrawer(drawerList);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        try {
            userInfo = new JSONObject(getIntent().getStringExtra("test"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUserInfo(JSONObject obj) {
        userInfo = obj;
    }

    /* special lifecycle event; need to sync drawer listener here to display proper icon */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        //sync the drawerToggle state
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        //update drawer listener to the new configuration
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //first, try forwarding options 'click' to drawer toggle
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        //by default, just call the superclass
        return super.onOptionsItemSelected(item);
    }

    /* handles log out even */
    public void logout(){
        SQLiteHandler sessionHandler = new SQLiteHandler(this);
        sessionHandler.turnOffSessions();

        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


}


