package com.sleepeasy;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

/**
 * Author: Andrew Reid modified by Michael Fesser (added the urgent icon)
 * Date: Feb 15. 2015
 * Purpose:  This is the custom list adapter used to display the list of tickets.  It is used for both,
 * my tickets and department tickets.  It provides useful information to the user.
 */
public class RoomListAdapter extends ArrayAdapter<Room> {
    Context mContext;
    ArrayList<Room> mRooms;

    public RoomListAdapter(Context context,ArrayList<Room> rooms) {
        super(context,R.layout.room_row,rooms);
        mContext = context;
        mRooms = rooms;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       ViewHolder holder;

        if(convertView ==null){
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.room_row,null);
            holder = new ViewHolder();
            holder.roomNumber = (TextView)convertView.findViewById(R.id.roomNumber);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }
        Room room = mRooms.get(position);

        String currentRoom = room.getRoomNumber();
        holder.roomNumber.setText(currentRoom);


        return convertView;

    }

    private static class ViewHolder {
        TextView roomNumber;
    }
}
