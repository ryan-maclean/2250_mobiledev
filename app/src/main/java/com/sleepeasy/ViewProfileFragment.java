package com.sleepeasy;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sleepeasy.activities.AsyncResponse;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViewProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViewProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewProfileFragment extends Fragment implements AsyncResponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView profileName;
    private EditText name;
    private EditText address;
    private EditText phone;
    private EditText email;
    private EditText balance;
    private EditText cardType;
    private TextView labelAccountInfo;
    private TextView labelCardType;
    private TextView labelBalance;
    private Button edit;
    private Button save;
    private Button cancel;

    private static JSONObject userInfo= new JSONObject();

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViewProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewProfileFragment newInstance(String param1, String param2) {
        ViewProfileFragment fragment = new ViewProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ViewProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_view_profile, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        profileName = (TextView) getActivity().findViewById(R.id.profileName);
        name = (EditText) getActivity().findViewById(R.id.name);
        address = (EditText) getActivity().findViewById(R.id.address);
        phone = (EditText) getActivity().findViewById(R.id.phone);
        email = (EditText) getActivity().findViewById(R.id.email);
        balance = (EditText) getActivity().findViewById(R.id.balance);
        cardType = (EditText) getActivity().findViewById(R.id.cardType);
        labelBalance = (TextView) getActivity().findViewById(R.id.labelBalance);
        labelCardType = (TextView) getActivity().findViewById(R.id.labelCardType);
        labelAccountInfo = (TextView) getActivity().findViewById(R.id.labelAccountInfo);
        edit = (Button) getActivity().findViewById(R.id.edit);
        save = (Button) getActivity().findViewById(R.id.save);
        cancel = (Button) getActivity().findViewById(R.id.cancel);

        try {




            profileName.setText(userInfo.getString("username") + "s Profile");
            name.setText(userInfo.getString("first_name") + " " + userInfo.getString("last_name"));
            address.setText(userInfo.getString("address") + ", " + userInfo.getString("city")+ "\n" + userInfo.getString("postal_code"));
            phone.setText(userInfo.getString("phone"));
            email.setText(userInfo.getString("email"));
            balance.setText(userInfo.getString("balance"));
            cardType.setText(userInfo.getString("card_type"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        populateFields();
        toggleEditFields(false);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleEditFields(true);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(v);
            }

        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleEditFields(false);
                populateFields();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private String getTextString(EditText field) {
        return field.getText().toString();
    }

    private void save(View v) {
        try {
            JSONObject update = new JSONObject();
            String sName = getTextString(name);
            String sAddress = getTextString(address);
            String sPhone = getTextString(phone);
            String sEmail = getTextString(email);

            update.put("id", Integer.parseInt(userInfo.getString("id")));
            update.put("first_name", sName.substring(0, sName.indexOf(" ")));
            update.put("last_name", sName.substring(sName.indexOf(" ")));
            update.put("address", sAddress.substring(0, sAddress.indexOf(",")));
            update.put("city", sAddress.substring(sAddress.indexOf(",") + 1, sAddress.indexOf("\n")));
            update.put("postal_code", sAddress.substring(sAddress.indexOf("\n")));
            update.put("phone", sPhone);
            update.put("email", sEmail);

            Log.d("RMACLEAN", update.getString("first_name"));
            Log.d("RMACLEAN", update.getString("last_name"));
            Log.d("RMACLEAN", update.getString("address"));
            Log.d("RMACLEAN", update.getString("city"));
            Log.d("RMACLEAN", update.getString("postal_code"));
            Log.d("RMACLEAN", update.getString("phone"));
            Log.d("RMACLEAN", update.getString("email"));

            JSONPost connect = new JSONPost(getResources().getString(R.string.url) + "updateprofile.php");

            // From HelmiB, toobsco42 http://stackoverflow.com
            connect.delegate = this;

            connect.execute(new JSONObject[]{update});

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void toggleEditFields(boolean enable) {
        name.setEnabled(enable);
        address.setEnabled(enable);
        phone.setEnabled(enable);
        email.setEnabled(enable);
        if (enable == true) {
            labelAccountInfo.setVisibility(View.GONE);
            labelCardType.setVisibility(View.GONE);
            cardType.setVisibility(View.GONE);
            labelBalance.setVisibility(View.GONE);
            balance.setVisibility(View.GONE);
            edit.setVisibility(View.GONE);
            save.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        } else {
            labelAccountInfo.setVisibility(View.VISIBLE);
            labelCardType.setVisibility(View.VISIBLE);
            cardType.setVisibility(View.VISIBLE);
            labelBalance.setVisibility(View.VISIBLE);
            balance.setVisibility(View.VISIBLE);
            edit.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
            cancel.setVisibility(View.GONE);
        }
    }

    private void populateFields() {
        try {
            profileName.setText(userInfo.getString("username") + " Profile");
            name.setText(userInfo.getString("first_name") + " " + userInfo.getString("last_name"));
            address.setText(userInfo.getString("address") + ", " + userInfo.getString("city") + "\n" + userInfo.getString("postal_code"));
            phone.setText(userInfo.getString("phone"));
            email.setText(userInfo.getString("email"));
            balance.setText(userInfo.getString("balance"));
            cardType.setText(userInfo.getString("card_type"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void processFinish(JSONObject jsonResponse) {
        try {
            JSONObject update = new JSONObject(jsonResponse.getString("update"));
            // JSONObject error = new JSONObject(jsonResponse.getString("databaseError"));
            Log.d("RMACLEAN",jsonResponse.getString("databaseError"));

            if(jsonResponse.getBoolean("databaseError")== false && update.length() > 0){
                userInfo = update;
                MainActivity m = (MainActivity) getActivity();
                m.setUserInfo(userInfo);
                toggleEditFields(false);
                populateFields();
            }

        } catch (Exception e) {

        }
    }
    public static JSONObject userInfo(String info){


        try {
            userInfo = new JSONObject(info);
        } catch (JSONException e) {
            e.printStackTrace();
            userInfo = new JSONObject();
        }

        return userInfo;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    class JSONPost extends AsyncTask<JSONObject, JSONObject, JSONObject> {

        private String url = "";
        public AsyncResponse delegate = null;
        private JSONObject jsonResponse = null;

        public JSONPost(String url) {
            Log.d("RYANTEST", url);
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected JSONObject doInBackground(JSONObject... data) {
            try {
                JSONObject json = data[0];
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);
                HttpPost post = new HttpPost(url);

                StringEntity se = new StringEntity("json=" + json.toString());
                post.addHeader("content-type", "application/x-www-form-urlencoded");
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                String resultFromServer = EntityUtils.toString(response.getEntity());
                Log.d("JSTILLMAN",resultFromServer);
                jsonResponse = new JSONObject(resultFromServer);
                //  jsonResponse = new JSONObject(resultFromServer);
                jsonResponse.put("databaseError", false);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    jsonResponse = new JSONObject();
                    jsonResponse.put("databaseError", true);
                    jsonResponse.put("databaseErrorText", "There is no connection to the database, contact the administrator.");
                } catch (Exception db) {
                    db.printStackTrace();
                }
            } finally {
                Log.d("JSON RESPONSE:" , jsonResponse.toString());
                return jsonResponse;

            }
        }

        // From HelmiB, toobsco42 http://stackoverflow.com
        @Override
        protected void onPostExecute(JSONObject jsonResponse) {
            try {
                delegate.processFinish(jsonResponse);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}
