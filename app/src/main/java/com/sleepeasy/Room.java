package com.sleepeasy;

/**
 * Created by jstillman on 2/19/2015.
 */
public class Room {

    String roomNumber;

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }
}
