package com.sleepeasy.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sleepeasy.MainActivity;
import com.sleepeasy.R;
import com.sleepeasy.Room;
import com.sleepeasy.RoomListAdapter;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RoomList extends ActionBarActivity implements AsyncResponse {
    //private
    private ListView listView;
    private ArrayAdapter<Room> listAdapter;
    JSONObject userInfoPub;
    JSONObject roomListPub;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_room_list);
       JSONObject roomListJsonPublic = null;
        final JSONObject userInfoPublic = null;
        JSONObject  roomListJson= null;

        try {
           roomListJson = new JSONObject(getIntent().getStringExtra("results"));

             JSONObject  userInfo = new JSONObject(getIntent().getStringExtra("info"));
           // setRoomList(roomListJson);
            setuserInfo(userInfo);
            setRoomListPub(roomListJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<Room> rooms = new ArrayList<Room>();
        try {
            JSONArray jsonArrayRooms = roomListJson.getJSONArray("rooms");

            for(int i = 0; i<jsonArrayRooms.length();i++){

                Room room = new Room();
                room.setRoomNumber(jsonArrayRooms.get(i).toString());
                rooms.add(room);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        listView =  (ListView)findViewById(R.id.list);
//


       RoomListAdapter adapter = new RoomListAdapter(getApplicationContext(),rooms);
     listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               Room room = (Room)listView.getItemAtPosition(position);

               // Toast.makeText(getApplicationContext(),room.getRoomNumber(),Toast.LENGTH_LONG).show();
                String roomNumber = room.getRoomNumber();

                String parseRoomNumber = roomNumber.replace("[", "");
               final String roomNumberText = parseRoomNumber.replace("]","");


            //http://stackoverflow.com/questions/5127407/how-to-implement-a-confirmation-yes-no-dialogpreference

                new AlertDialog.Builder(RoomList.this)
                        .setTitle("Book Room")
                        .setMessage("Would you like to book this room?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                // JSON OBJECT STUFF
                                try {
                                  int userId = userInfoPub.getInt("id");
                                  int accountId = userInfoPub.getInt("account_id");
                                  int roomType = Integer.parseInt(roomListPub.getString("roomType"));
                                  String roomNumber = roomNumberText;
                                  String checkin  = roomListPub.getString("checkin");
                                  String checkout = roomListPub.getString("checkout");
                                  String numGuest = roomListPub.getString("numberOfGuests");



                                    try {
                                        Log.d("RYANTEST", "Create JSONObject");
                                        JSONObject login = new JSONObject();
                                        login.put("user_id",userId );
                                        login.put("account_id",accountId);
                                        login.put("checkin",checkin);
                                        login.put("checkout",checkout);
                                        login.put("num_of_guests",numGuest);
                                        login.put("room_type",roomType);
                                        login.put("room_number",roomNumber);
                                        //login.put("password", password);

                                        JSONPost connect = new JSONPost(getResources().getString(R.string.url) + "bookRoom.php");

                                        // From HelmiB, toobsco42 http://stackoverflow.com
                                        connect.delegate = RoomList.this;

                                        connect.execute(new JSONObject[]{login});

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }})
                        .setNegativeButton(android.R.string.no, null).show();





              ///  Toast.makeText(getApplicationContext(),parseRoomNumber,Toast.LENGTH_LONG).show();


            }
        });




    }

    private void setuserInfo(JSONObject userInfo) {
       userInfoPub = userInfo;

    }

    private JSONObject getUserInfo(){
            return userInfoPub;

}

    private void setRoomListPub(JSONObject roomList) {
        roomListPub = roomList;

    }

    private JSONObject getRoomListPub(){
        return roomListPub;

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_room_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(JSONObject jsonResponse) {
       Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);

        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, "Sleepeasy@sleepeasy.com");
        intent.putExtra(ContactsContract.Intents.Insert.NAME , "SleepEasy Hotels");
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, "1-902-SLE-EASY");
        startActivity(intent);

        Toast.makeText(getApplicationContext(),"Once Contact Added, Click back to Search Again",Toast.LENGTH_LONG).show();


    }

    @Override
    protected void onResume() {
       super.onResume();
//        Intent intent = new Intent(this,MainActivity.class);
//
//        startActivity(intent);

    }

    class JSONPost extends AsyncTask<JSONObject, JSONObject, JSONObject> {

        private String url = "";
        public AsyncResponse delegate = null;
        private JSONObject jsonResponse = null;

        public JSONPost(String url) {
            Log.d("RYANTEST", url);
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected JSONObject doInBackground(JSONObject... data) {
            try {
                JSONObject json = data[0];
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);
                HttpPost post = new HttpPost(url);

                StringEntity se = new StringEntity("json=" + json.toString());
                post.addHeader("content-type", "application/x-www-form-urlencoded");
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                String resultFromServer = EntityUtils.toString(response.getEntity());
                Log.d("JSTILLMAN",resultFromServer);
                jsonResponse = new JSONObject(resultFromServer);
                //  jsonResponse = new JSONObject(resultFromServer);
                jsonResponse.put("databaseError", false);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    jsonResponse = new JSONObject();
                    jsonResponse.put("databaseError", true);
                    jsonResponse.put("databaseErrorText", "There is no connection to the database, contact the administrator.");
                } catch (Exception db) {
                    db.printStackTrace();
                }
            } finally {
                Log.d("JSON RESPONSE:" , jsonResponse.toString());
                return jsonResponse;

            }
        }

        // From HelmiB, toobsco42 http://stackoverflow.com
        @Override
        protected void onPostExecute(JSONObject jsonResponse) {
            try {
                delegate.processFinish(jsonResponse);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}
