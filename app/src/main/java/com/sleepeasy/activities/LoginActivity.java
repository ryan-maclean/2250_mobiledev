package com.sleepeasy.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sleepeasy.MainActivity;
import com.sleepeasy.R;
import com.sleepeasy.SQLiteHandler;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginActivity extends ActionBarActivity implements AsyncResponse{

    private Button login;

   String test;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //if session exists, populate fields
        SQLiteHandler sessionHandler = new SQLiteHandler(this);

        if(sessionHandler.isActive()){
            String[] userInfo = sessionHandler.getActiveUser();
            String username = userInfo[0];
            String password = userInfo [0];

            Toast.makeText(getApplicationContext(),username,Toast.LENGTH_LONG).show();

            EditText usernameField = (EditText) findViewById(R.id.fieldUsername);
            EditText passwordField = (EditText) findViewById(R.id.fieldPassword);

            usernameField.setText(username);
            passwordField.setText(password);
        }



        login = (Button) findViewById(R.id.buttonLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("RYANTEST", "LoginClicked");
                loginUser(v);
            }
        });
    }

    public static String sha1Encode(String password) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(password.getBytes());
        StringBuffer hashedPassword = new StringBuffer();

        for (int i = 0; i < result.length; i++) {
            hashedPassword.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return hashedPassword.toString();
    }

    public void loginUser(View view) {
        Log.d("RYANTEST", "loginUser()");
        String username = ((EditText) findViewById(R.id.fieldUsername)).getText().toString();
        String password = ((EditText) findViewById(R.id.fieldPassword)).getText().toString();
        //Hashing the password using SHA1 Encryption
//        try {
//            password = sha1Encode(((EditText) findViewById(R.id.fieldPassword)).getText().toString());
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }


        // Use AsyncTask execute Method To Prevent ANR Problem
        try {
            Log.d("RYANTEST", "Create JSONObject");
            JSONObject login = new JSONObject();
            login.put("username", username);
            login.put("password", password);

            JSONPost connect = new JSONPost(getResources().getString(R.string.url) + "login.php");

            // From HelmiB, toobsco42 http://stackoverflow.com
            connect.delegate = this;

            connect.execute(new JSONObject[]{login});

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void processFinish(JSONObject jsonResponse) {
        try {
            JSONObject login = new JSONObject(jsonResponse.getString("login"));
           // JSONObject error = new JSONObject(jsonResponse.getString("databaseError"));
            Log.d("JSTILLMAN",jsonResponse.getString("databaseError"));

            if(jsonResponse.getBoolean("databaseError")== false){
                //turn off existing sessions & create new
                SQLiteHandler sessionHandler = new SQLiteHandler(this);

                sessionHandler.turnOffSessions();

                String username = login.getString("username");
                String password = login.getString("password");

                if (!sessionHandler.userExists(username)) { sessionHandler.addUser(username, password); }
                sessionHandler.turnOnSession(username);

                //go to main activity
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra("test",login.toString());
                startActivity(intent);
            }else{
                Toast.makeText(LoginActivity.this, "Login Information not Correct.", Toast.LENGTH_LONG).show();
            }

        } catch (Exception e) {

        }
    }
//Taken from https://mongskiewl.wordpress.com/2013/10/16/sending-json-data-from-android-to-a-php-script/

    class JSONPost extends AsyncTask<JSONObject, JSONObject, JSONObject> {

        private String url = "";
        public AsyncResponse delegate = null;
        private JSONObject jsonResponse = null;

        public JSONPost(String url) {
            Log.d("RYANTEST", url);
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected JSONObject doInBackground(JSONObject... data) {
            try {
                JSONObject json = data[0];
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);
                HttpPost post = new HttpPost(url);

                StringEntity se = new StringEntity("json=" + json.toString());
                post.addHeader("content-type", "application/x-www-form-urlencoded");
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                String resultFromServer = EntityUtils.toString(response.getEntity());
                Log.d("JSTILLMAN",resultFromServer);
                jsonResponse = new JSONObject(resultFromServer);
              //  jsonResponse = new JSONObject(resultFromServer);
                jsonResponse.put("databaseError", false);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    jsonResponse = new JSONObject();
                    jsonResponse.put("databaseError", true);
                    jsonResponse.put("databaseErrorText", "There is no connection to the database, contact the administrator.");
                } catch (Exception db) {
                    db.printStackTrace();
                }
            } finally {
               Log.d("JSON RESPONSE:" , jsonResponse.toString());
                return jsonResponse;

            }
        }

        // From HelmiB, toobsco42 http://stackoverflow.com
        @Override
        protected void onPostExecute(JSONObject jsonResponse) {
            try {
                delegate.processFinish(jsonResponse);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
}

