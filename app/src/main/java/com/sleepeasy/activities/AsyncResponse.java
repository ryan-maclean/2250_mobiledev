package com.sleepeasy.activities;

import org.json.JSONObject;

/**
 * Created by dmaclean31977 on 2/13/2015.
 */
public interface AsyncResponse {
    void processFinish(JSONObject jsonResponse);
}