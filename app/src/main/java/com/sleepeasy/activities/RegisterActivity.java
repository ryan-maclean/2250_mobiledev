package com.sleepeasy.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sleepeasy.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class RegisterActivity extends ActionBarActivity implements AsyncResponse {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Spinner spinner = (Spinner) findViewById(R.id.paymentType);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.paymentTypes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //This method takes the registration details entered by the user, sends them off to be validated,
    // it forwards them to the login page is valid.
    public void register(View view) {
        String errorString = "";
        int errorLevel = 0;
        String userName = ((EditText) findViewById(R.id.userName)).getText().toString();
        String password = ((EditText) findViewById(R.id.password)).getText().toString();
        String email = ((EditText) findViewById(R.id.email)).getText().toString();
        String firstName = ((EditText) findViewById(R.id.firstName)).getText().toString();
        String lastName = ((EditText) findViewById(R.id.lastName)).getText().toString();
        String address = ((EditText) findViewById(R.id.address)).getText().toString();
        String city = ((EditText) findViewById(R.id.city)).getText().toString();
        String postalCode = ((EditText) findViewById(R.id.postalCode)).getText().toString();
        String phone = ((EditText) findViewById(R.id.homePhone)).getText().toString();
        String paymentType = ((Spinner) findViewById(R.id.paymentType)).getSelectedItem().toString();
        String cardName = ((EditText) findViewById(R.id.cardholderName)).getText().toString();
        String cardNo = ((EditText) findViewById(R.id.cardNumber)).getText().toString();
        String expiry = ((DatePicker) findViewById(R.id.expiry)).getYear()  + "-"+ ((DatePicker)findViewById(R.id.expiry)).getMonth()+"-1";



        if (userName.length() == 0) {
            errorString += "You need to enter your UserName\n";
            errorLevel = 1;
        }
        if (password.length() == 0) {
            errorString += "You need to enter your password\n";
            errorLevel = 1;
        }
        if (email.length() == 0) {
            errorString += "You need to confirm your email\n";
            errorLevel = 1;
        }

        if (firstName.length() == 0) {
            errorString += "You need to confirm your first name\n";
            errorLevel = 1;
        }
        if (lastName.length() == 0) {
            errorString += "You need to confirm your last name\n";
            errorLevel = 1;
        }
        if (address.length() == 0) {
            errorString += "You need to confirm your address\n";
            errorLevel = 1;
        }

        if (city.length() == 0) {
            errorString += "You need to confirm your city\n";
            errorLevel = 1;
        }
        if (postalCode.length() == 0) {
            errorString += "You need to confirm your postal code\n";
            errorLevel = 1;
        }
        if (paymentType.length()==0) {
            errorString += "You need to confirm your payment type\n";
            errorLevel = 1;
        }
        if (cardName.length() == 0) {
            errorString += "You need to confirm your cardholder name\n";
            errorLevel = 1;
        }
        if (cardNo.length() == 0) {
            errorString += "You need to confirm your card number\n";
            errorLevel = 1;
        }

        if (errorLevel == 1) {
            Toast.makeText(this, errorString, Toast.LENGTH_LONG).show();
        }

        if (errorLevel == 0) {
//            //Hashing the password using SHA1 Encryption
//            try {
//                password = sha1Encode(password);
//            } catch (NoSuchAlgorithmException e) {
//                e.printStackTrace();
//            }

            // Use AsyncTask execute Method To Prevent ANR Problem
            try {
                JSONObject register = new JSONObject();
                register.put("username", userName);
                register.put("password", password);
                register.put("email", email);
                register.put("first_name", firstName);
                register.put("last_name", lastName);
                register.put("address", address);
                register.put("city", city);
                register.put("postal_code", postalCode);
                register.put("phone",phone);
                register.put("payment_type", paymentType);
                register.put("cardholder_name", cardName);
                register.put("card_number", cardNo);
                register.put("card_expiration", expiry);


                JSONPost connect = new JSONPost(getResources().getString(R.string.url)+ "register.php");
                // From HelmiB, toobsco42 http://stackoverflow.com
                connect.delegate = this;

                connect.execute(new JSONObject[]{register});

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    //Built from https://www.youtube.com/watch?v=KPJXecFdEDo video tutorial
    //This method Accepts a string for a password, encrypts it into hex using SHA1, and returns the hex encoded string.
    private String sha1Encode(String password) throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(password.getBytes());
        StringBuffer hashedPassword = new StringBuffer();

        for (int i = 0; i < result.length; i++) {
            hashedPassword.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return hashedPassword.toString();
    }

    // Part from From HelmiB, toobsco42 http://stackoverflow.com
    @Override
    public void processFinish(JSONObject jsonResponse) {

    }

    class JSONPost extends AsyncTask<JSONObject, JSONObject, JSONObject> {

        private String url = "";
        public AsyncResponse delegate = null;
        private JSONObject jsonResponse = null;

        public JSONPost(String url) {
            Log.d("RYANTEST", url);
            this.url = url;
        }

        @Override
        protected JSONObject doInBackground(JSONObject... data) {
            try {
                JSONObject json = data[0];
                HttpClient client = new DefaultHttpClient();
                HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);
                HttpPost post = new HttpPost(url);

                StringEntity se = new StringEntity("json=" + json.toString());
                post.addHeader("content-type", "application/x-www-form-urlencoded");
                post.setEntity(se);

                HttpResponse response = client.execute(post);
                String resultFromServer = EntityUtils.toString(response.getEntity());
                Log.d("JSTILLMAN",resultFromServer);
                jsonResponse = new JSONObject(resultFromServer);
                jsonResponse = new JSONObject(resultFromServer);
                jsonResponse.put("databaseError", false);
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    jsonResponse = new JSONObject();
                    jsonResponse.put("databaseError", true);
                    jsonResponse.put("databaseErrorText", "There is no connection to the database, contact the administrator.");
                } catch (Exception db) {
                    db.printStackTrace();
                }
            } finally {
                return jsonResponse;
            }
        }

        @Override
        protected void onPostExecute(JSONObject jsonResponse) {
            try {
                Log.d("JSTILLMAN","IN PRoccessFinish");
                // From From HelmiB, toobsco42 http://stackoverflow.com
                JSONObject registerDetails;
                registerDetails = new JSONObject(jsonResponse.getString("response"));
                Log.d("JSTILLMAN", String.valueOf(registerDetails.getInt("registerStatus")));
                // Forward to correct screen
                if (registerDetails.getInt("registerStatus") == 1) {


                    Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                } else if (registerDetails.getInt("registerStatus") == 0) {
                    Toast.makeText(RegisterActivity.this, "That username is already in use.", Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}



